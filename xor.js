/**
 Here is a string of bytes encoded in hex:
3d2e212b20226f3c2a2a2b
These bytes have been encrypted with a cunning cipher: each character has been XOR'd with 79 (decimal). In cryptography, '79' is referred to as the 'key'.
To decrypt it, perform the same XOR again. This will reveal your answer.
 */

const xor = str => {
  let s = ''
  const bytes = []
  for (var i = 0; i < str.length; i += 2) {
    bytes.push('0x' + str.substr(i, 2))
  }
  bytes.forEach(b => {
    const xored = parseInt(b, 16) ^ 79
    console.log(b, xored, String.fromCharCode(xored))
    s += String.fromCharCode(xored)
  })
  return s
}

console.log(xor('3d2e212b20226f3c2a2a2b'))
